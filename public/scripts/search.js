$('#campground-search').on('input', function() {
  var search = $(this).serialize();
  if(search === "search=") {
    search = "all"
  }
  $.get('/campgrounds?' + search, function(data) {
    $('#campground-grid').html('');
    data.forEach(function(campground) {
      $('#campground-grid').append(`
        <div class="card">
          <a href="/campgrounds/${ campground._id }"><img class="card-img" src="${ campground.image }" alt="${ campground.name }"></a>
          <div class="card-body">
            <a href="/campgrounds/${ campground._id }"><p class="card-title text-center">${ campground.name }</p></a>
          </div>
        </div>
      `);
    });
  });
});

$('#campground-search').submit(function(event) {
  event.preventDefault();
});